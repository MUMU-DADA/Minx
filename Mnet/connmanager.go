package Mnet

import (
	"fmt"
	"gitee.com/MUMU-DADA/Minx/Miface"
	"gitee.com/MUMU-DADA/Minx/Mutils"
	"sync"
)

/*
	用于实现控制连接connManager的接口
*/
// connManager所需的结构体
type ConnManager struct {
	ConnPoolMap map[int]Miface.Iconnect
	b           sync.RWMutex
}

// 用于创建连接控制器的一个实例
func NewConnManager() Miface.IConnManager {
	a := &ConnManager{
		ConnPoolMap: make(map[int]Miface.Iconnect),
	}
	fmt.Printf("[systemLog]:连接控制池最大数量为%d个\n", Mutils.GlobalValue.ServerMaxConn)
	fmt.Println("[systemLog]:连接控制池创建成功")
	return a
}

// 增加连接
func (cm *ConnManager) ConnManAdd(connIn Miface.Iconnect) error {
	id := connIn.GetConnID()
	// 新增为写操作 需要先加写锁
	cm.b.Lock()
	defer cm.b.Unlock()
	cm.ConnPoolMap[int(id)] = connIn
	return nil

}

// 删除连接
func (cm *ConnManager) ConnManDel(connIn Miface.Iconnect) {
	// 删除为写操作 需要先加写锁
	cm.b.Lock()
	defer cm.b.Unlock()

	cm.ConnPoolMap[int(connIn.GetConnID())].Getconn().Close()
	delete(cm.ConnPoolMap, int(connIn.GetConnID()))
}

// 中断所有的连接
func (cm *ConnManager) ConnManDelAll() {
	// 删除为写操作 需要先加写锁
	cm.b.Lock()
	defer cm.b.Unlock()

	for k, v := range cm.ConnPoolMap {
		v.Getconn().Close()
		delete(cm.ConnPoolMap, k)
	}

}

// 根据连接ID获取连接
func (cm *ConnManager) ConnManGetID(idIn int) Miface.Iconnect {
	// 查询为读操作 需要先加读锁
	cm.b.RLock()
	defer cm.b.RUnlock()
	return cm.ConnPoolMap[idIn]
}

// 获取当前总连接数量
func (cm *ConnManager) ConnManGetNum() int {
	// 查询为读操作 需要先加读锁
	cm.b.RLock()
	defer cm.b.RUnlock()
	return len(cm.ConnPoolMap)
}
