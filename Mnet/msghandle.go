package Mnet

import (
	"errors"
	"fmt"
	"gitee.com/MUMU-DADA/Minx/Miface"
	"gitee.com/MUMU-DADA/Minx/Mutils"
	"strconv"
)

/*
	用于实现msgHandle的接口
*/
type MsgHandle struct {
	// 使用map来存放所有的message所对应使用router路由方法
	MsgHandleMap map[uint64]Miface.IRouter
	// 定义一组消息队列 因为是一组 所以使用切片
	MsgHandleDuilie []chan Miface.Irequest
	// 定义工作池的工作单元个数
	MsgHandleWorkUnitNum int
}

// 初始化创建一个管理路由MsgHandle实例的函数
func NewMsgHandle() Miface.IMsgHandle {
	a := &MsgHandle{
		MsgHandleMap:         make(map[uint64]Miface.IRouter),
		MsgHandleWorkUnitNum: Mutils.GlobalValue.ServerWorkUnitNum,
		MsgHandleDuilie:      make([]chan Miface.Irequest, Mutils.GlobalValue.ServerWorkUnitNum),
	}
	return a
}

// 调度路由Router方法
func (mh *MsgHandle) RouterUse(request Miface.Irequest) error {
	if mh.MsgHandleMap[request.GetID()] == nil {
		fmt.Println("[systemLog]:当前请求ID" + strconv.Itoa(int(request.GetID())) + "所属路由不存在")
		return errors.New("[systemLog]:当前请求ID" + strconv.Itoa(int(request.GetID())) + "所属路由不存在")
	}
	mh.MsgHandleMap[request.GetID()].HandleQian(request)
	mh.MsgHandleMap[request.GetID()].Handle(request)
	mh.MsgHandleMap[request.GetID()].HandleHou(request)
	return nil
}

// 添加路由Router方法
func (mh *MsgHandle) RouterAdd(msgid uint64, router Miface.IRouter) error {
	if mh.MsgHandleMap[msgid] != nil {
		fmt.Println("[systemLog]:当前注册路由ID" + strconv.Itoa(int(msgid)) + "已被注册,无法完成注册")
		return errors.New("当前注册路由ID" + strconv.Itoa(int(msgid)) + "已被注册,无法完成注册")
	}
	mh.MsgHandleMap[msgid] = router
	return nil
}

// 创建一个工作池并使用生成工作池函数来生成工作单元协程
func (mh *MsgHandle) StartWorkPool() {
	fmt.Printf("[systemLog]:启动工作池队列\n[systemLog]:设置工作池数量 %d 个\n[systemLog]:设置队列数量 %d 个\n", Mutils.GlobalValue.ServerWorkUnitNum, Mutils.GlobalValue.ServerWorkDuilie)
	for i := 0; i < Mutils.GlobalValue.ServerWorkUnitNum; i++ {
		fmt.Printf("[systemLog]:启动工作单元%d\n", i)
		go mh.StartWorkUnit(i)
	}
}

// 工作单元go程 接收传来的工作request请求 与工作单元id 并调用MsgHandle的调度路由方法来使用request
func (mh *MsgHandle) StartWorkUnit(idIn int) {
	mh.MsgHandleDuilie[idIn] = make(chan Miface.Irequest, Mutils.GlobalValue.ServerWorkDuilie)
	defer close(mh.MsgHandleDuilie[idIn])
	for {
		select {
		case request := <-mh.MsgHandleDuilie[idIn]:
			e := mh.RouterUse(request)
			if e != nil {
				fmt.Printf("[systemLog]:ID:%d 工作单元执行%d 路由失败 %v\n", idIn, request.GetID(), e)
				break
			}
		}

	}
}

// var sendWorkId int = 0
// var sendWorkIdlock sync.Mutex

// 将收到的request发送给对应的工作池 并做简易负载均衡
func (mh *MsgHandle) SendRequestToWorkPool(request Miface.Irequest) {
	sendWorkId := request.GetID() % uint64(mh.MsgHandleWorkUnitNum)
	fmt.Printf("[systemLog]:将连接ID:%d 请求ID:%d 分配给WorkUnitID:%d 运行\n", request.GetConnect().GetConnID(), request.GetID(), sendWorkId)
	mh.MsgHandleDuilie[sendWorkId] <- request
}
