package Mnet

import "gitee.com/MUMU-DADA/Minx/Miface"

// 解决Irequest接口的方法实现

// 定义request结构体
type Request struct {
	conn Miface.Iconnect
	data Miface.Imessage
}

// 获取request连接
func (r *Request) GetConnect() Miface.Iconnect {
	return r.conn
}

// 获取request二进制流数据
func (r *Request) GetData() []byte {
	return r.data.GetMsg()
}

// 获取request头长度
func (r *Request) GetLen() uint64 {
	return r.data.GetLen()
}

// 获取request头ID
func (r *Request) GetID() uint64 {
	return r.data.GetID()
}
