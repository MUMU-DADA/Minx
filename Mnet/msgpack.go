package Mnet

import (
	"bytes"
	"encoding/binary"
	"gitee.com/MUMU-DADA/Minx/Miface"
)

/*
	用于实现Pack抽象接口层
*/

// 定义pack的空结构体 用于实例化对象时调用其方法
type Pack struct{}

// 实例化一个pack对象的方法
func NewPack() *Pack {
	return &Pack{}
}

// 获取包的长度
func (p *Pack) GetPackLen() uint64 {
	// 包ID的uint64加上包ID的uint64 一共16个字节
	return 16
}

// 打包
func (p *Pack) Pack(msg Miface.Imessage) ([]byte, error) {
	temp := bytes.NewBuffer([]byte{})

	// 将整体压入
	l := msg.GetLen()
	e := binary.Write(temp, binary.BigEndian, l)
	if e != nil {
		return nil, e
	}

	// 将包ID压入
	id := msg.GetID()
	e = binary.Write(temp, binary.BigEndian, id)
	if e != nil {
		return nil, e
	}

	// 将包内容压入
	data := msg.GetMsg()
	e = binary.Write(temp, binary.BigEndian, data)
	if e != nil {
		return nil, e
	}

	// 将压制好的包传回
	return temp.Bytes(), nil

}

// 解包
func (p *Pack) UnPack(waitToUnpackData []byte) (Miface.Imessage, error) {

	dataReader := bytes.NewReader(waitToUnpackData)

	msg := &Message{}

	e := binary.Read(dataReader, binary.BigEndian, &msg.MsgLen)
	if e != nil {
		return nil, e
	}
	e = binary.Read(dataReader, binary.BigEndian, &msg.MsgID)
	if e != nil {
		return nil, e
	}
	return msg, nil

}
