package Mnet

import "gitee.com/MUMU-DADA/Minx/Miface"

// 用于定义路由Irouter的接口函数的实体实现层

// 创建一个路由结构的基类 后续用户依靠继承重写来使用
type BaseRouter struct{}

// 处理业务前
func (rt *BaseRouter) HandleQian(request Miface.Irequest) {}

// 处理业务
func (rt *BaseRouter) Handle(request Miface.Irequest) {}

// 处理业务后
func (rt *BaseRouter) HandleHou(request Miface.Irequest) {}
