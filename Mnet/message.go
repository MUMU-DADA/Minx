package Mnet

import "gitee.com/MUMU-DADA/Minx/Miface"

/*
	用于实现Imessage抽象层
*/

// 定义Message的结构体
type Message struct {
	MsgLen uint64
	MsgID  uint64
	Msg    []byte
}

func NewMessage(sendID uint64, sendData []byte) Miface.Imessage {
	msg := &Message{
		MsgLen: uint64(len(sendData)),
		MsgID:  sendID,
		Msg:    sendData,
	}
	return msg
}

// 获取消息的参数方面

// 获取消息的长度
func (msg *Message) GetLen() uint64 {
	return msg.MsgLen
}

// 获取消息的ID
func (msg *Message) GetID() uint64 {
	return msg.MsgID
}

// 获取消息的内容
func (msg *Message) GetMsg() []byte {
	return msg.Msg
}

// 设置消息的参数方面

// 设置消息的ID
func (msg *Message) SetMsgID(newID uint64) {
	msg.MsgID = newID
}

// 设置消息的内容
func (msg *Message) SetMsg(newMsg []byte) {
	msg.Msg = newMsg
	msg.MsgLen = uint64(len(newMsg))
}
