package Mnet

import (
	"errors"
	"fmt"
	"gitee.com/MUMU-DADA/Minx/Miface"
	"gitee.com/MUMU-DADA/Minx/Mutils"
	"net"
	"runtime"
	"strconv"
)

/*
	此处用于实现抽象层
*/

// 定义结构
type Server struct {
	Name        string
	IpVer       string
	Ip          string
	port        int
	url         string
	Channel     chan string
	ChannelStop chan bool
	// Router      Miface.IRouter
	MsgHandle Miface.IMsgHandle
	// 连接控制队列
	ConnManager Miface.IConnManager
}

// 初始化方法
func NewServer(ip string, port int) Miface.MServer {
	if ip != "" && port != 0 {
		// fmt.Println("有默认输入")
		newurl := ip + ":" + strconv.Itoa(port)
		newS := &Server{
			Name:        Mutils.GlobalValue.ServerName,
			IpVer:       Mutils.GlobalValue.ServerIPVer,
			Ip:          ip,
			port:        port,
			url:         newurl,
			Channel:     make(chan string),
			ChannelStop: make(chan bool),
			MsgHandle:   NewMsgHandle(),
		}
		return newS
	} else {
		newurl := Mutils.GlobalValue.ServerIP + ":" + strconv.Itoa(Mutils.GlobalValue.ServerPort)
		newS := &Server{
			Name:        Mutils.GlobalValue.ServerName,
			IpVer:       Mutils.GlobalValue.ServerIPVer,
			Ip:          Mutils.GlobalValue.ServerIP,
			port:        Mutils.GlobalValue.ServerPort,
			url:         newurl,
			Channel:     make(chan string),
			ChannelStop: make(chan bool),
			MsgHandle:   NewMsgHandle(),
		}
		return newS
	}
}

// 新增router的方法
func (s *Server) AddNewRouter(addid uint64, router Miface.IRouter) {
	s.MsgHandle.RouterAdd(addid, router)
	// fmt.Println("新增路由成功")
}

// 返回服务器所拥有的连接池
func (s *Server) GetServerConnManager() Miface.IConnManager {
	return s.ConnManager
}

// 启动方法
func (s *Server) Start() {
	fmt.Printf("欢迎使用Minx\n框架版本 :%v DEBUG :%v\n", Mutils.VERSION, Mutils.GlobalValue.DebugSw)
	// 创建一个监听GO协程
	go s.ServerGetConn()
	// 启动消息队列
	s.MsgHandle.StartWorkPool()
	// 启动连接池
	s.ConnManager = NewConnManager()
}

// 停止方法
func (s *Server) Stop() {
	s.ConnManager.ConnManDelAll()
	fmt.Println("[systemLog]:关闭服务器")
}

// 运行方法
func (s *Server) Run() {
	s.Start()
	select {}
	fmt.Println("监听结束!")
}

// 启动监听管道
func (s *Server) ServerChannelListen() {
	select {}
}

// 连接创建成功后的进行 阻塞 检测停止 并运行相应命令
func (s *Server) ServerDoOpen(getcon net.Conn) {
	// 创建一个连接关闭时关闭协程的协程
	go func() {
		temp := make([]byte, Mutils.GlobalValue.ServerRWByteLen)
		for {
			v, _ := getcon.Read(temp)
			if v == 0 {
				s.ChannelStop <- true
				defer getcon.Close()
				return
			}
		}
	}()

	// 检测到连接关闭时终止go程
	for {
		select {
		case <-s.ChannelStop:
			fmt.Println("检测到连接关闭!")
			runtime.Goexit()
		default:
			// 下面是执行动作语句协程
			s.ServerDoSomething(getcon)
		}
	}
}

// 连接运行命令
func (s *Server) ServerDoSomething(getcon net.Conn) {
	temp := make([]byte, 1024)
	v, e := getcon.Read(temp)
	if e != nil {
		fmt.Println("读取错误:", e)
		return
	}
	fmt.Printf("%s", temp[:v])
}

// 连接绑定执行命令一 :回写数据
func ConnHuixie(conn *net.TCPConn, msg []byte, v int) error {
	m := "[SystemCallBack]:" + string(msg[:v])
	if _, e := conn.Write([]byte(m)); e != nil {
		println("错误", e)
		return errors.New("回写错误")
	}
	return nil
}

// 启动一个简单的监听连接
func (s *Server) ServerGetConn() {
	newAddr, _ := net.ResolveTCPAddr(s.IpVer, s.url)
	newlisten, e := net.ListenTCP(s.IpVer, newAddr)
	if e != nil {
		fmt.Println("[systemLog]:监听错误!", e)
		return
	}
	fmt.Printf("[systemLog]:监听地址为:%v:%d\n", s.Ip, s.port)
	// 在监听结束后及时关闭所有的连接
	defer func() {
		e := newlisten.Close()
		if e != nil {
			fmt.Println("[systemLog]:关闭监听错误!", e)
			return
		}
		fmt.Println("[systemLog]:监听关闭")
	}()
	// 创建一个监听频道广播管道协程
	// go s.ServerChannelListen()

	// 连接ID初始化创建
	var connID int64 = 0

	for {
		newConn, e := newlisten.AcceptTCP()
		if e != nil {
			fmt.Println("[systemLog]:连接创建错误!", e)
			continue
		}
		// fmt.Println("[systemLog]:检测到连接")
		// 连接创建成功后开始进入连接方法携程
		// go s.ServerDoOpen(newConn)

		// 判断连接是否超出设定最大连接数量
		if s.ConnManager.ConnManGetNum() >= Mutils.GlobalValue.ServerMaxConn {
			newConn.Close()
			fmt.Println("[systemLog]:连接超出允许最大值")
			continue
		}

		// 使用写好的Connect方法来创建连接并绑定连接
		makeConn := NewConnect(s, newConn, connID, s.MsgHandle)
		go makeConn.Start()

		// 使用定义好的connmanager连接控制模块来控值连接 而不是直接创建连接
		// eadd := s.ConnManager.ConnManAdd(NewConnect(newConn, connID, s.MsgHandle))
		// if eadd != nil {
		//	fmt.Println(eadd)
		//	continue
		// }
		// go s.ConnManager.ConnManGetID(int(connID)).Start()

		// 连接ID自增
		connID++
	}
}
