package Mnet

import (
	"fmt"
	"gitee.com/MUMU-DADA/Minx/Miface"
	"gitee.com/MUMU-DADA/Minx/Mutils"
	"io"
	"net"
	"sync"
)

/*
	连接模块的功能实现编写
*/

type Connect struct {
	// 连接所属服务器
	ConnServer Miface.MServer
	// 连接句柄
	Conn *net.TCPConn
	// 连接ID
	ConnID int64
	// 连接状态
	Isclosed bool
	// 连接绑定的方法
	ConnAPI Miface.BangdinFunc
	// 检查当前连接是否已经终止的管道
	CheakExitC chan bool
	// 添加一个router
	// Router Miface.IRouter
	// 将之前的router单路由控制模式 转换成使用路由管理MsgHandle来控制 实现多路由管理
	CMsgHandle Miface.IMsgHandle
	// 创建一个给写进程发送数据的管道
	CtransData chan []byte
	// 增加连接属性的集合map
	cinfo map[string]interface{}
	// 保护属性的锁
	cinfolock sync.RWMutex
}

// 创建一个连接
func NewConnect(server Miface.MServer, getconn *net.TCPConn, getconnID int64, getMsgHandle Miface.IMsgHandle) Miface.Iconnect {
	newConnect := &Connect{
		ConnServer: server,
		Conn:       getconn,
		ConnID:     getconnID,
		Isclosed:   false,
		CheakExitC: make(chan bool, 1),
		CMsgHandle: getMsgHandle,
		CtransData: make(chan []byte),
		cinfo:      make(map[string]interface{}),
	}

	newConnect.ConnServer.GetServerConnManager().ConnManAdd(newConnect)
	return newConnect
}

// 启动的读数据子协程
func (c *Connect) StartReader() {
	// 协程执行完后 关闭连接
	defer c.Stop()
	// 创建一个拆包对象
	p := NewPack()

	for {
		select {
		case <-c.CheakExitC:
			return
		default:
			// 创建一个长度为定义长度的缓存
			temp := make([]byte, p.GetPackLen())
			// 从conn中将头部填充进temp
			v, e := io.ReadFull(c.Conn, temp)
			if v == 0 {
				fmt.Println("[systemLog]:读取到二进制流尾部 等待下一次发包")
				// c.CheakExitC <- true
				return
			}
			if e != nil {
				fmt.Println("[systemLog]:读取头字节错误", e)
				break
			}
			// 将temp解包进Message对象中
			newMsg, e1 := p.UnPack(temp)
			if e1 != nil {
				// 解包失败直接退出循环
				fmt.Println("[systemLog]:解包错误", e1)
				break
			}
			// 根据解包中的Message的len长度 开辟一个新的缓存
			temp2 := make([]byte, newMsg.GetLen())
			// 从conn中填充满temp2
			_, e2 := io.ReadFull(c.Conn, temp2)
			if e2 != nil {
				// 读取数据失败退出循环
				fmt.Println("[systemLog]:读取内容物错误", e2)
				break
			}
			/*
				finalMsg := newMsg.(*Message)
				finalMsg.Msg = make([]byte, finalMsg.MsgLen)
				finalMsg.Msg = temp2
			*/
			newMsg.SetMsg(temp2)

			req := &Request{
				conn: c,
				data: newMsg,
			}

			if Mutils.GlobalValue.ServerWorkUnitNum > 0 {
				// 使用路由队列来对请求进行路由调度
				c.CMsgHandle.SendRequestToWorkPool(req)
			} else {
				// 使用路由管理来对请求进行路由调度
				go c.CMsgHandle.RouterUse(req)
			}

			/*
				//开启一个进程来调用路由的各个功能
				go func(abc Miface.Irequest) {
					c.Router.HandleQian(abc)
					c.Router.Handle(abc)
					c.Router.HandleHou(abc)
				}(req)
			*/

		}
	}
}

// 提供一个发送封装好的Message数据包的方法
func (c *Connect) SendPackMsg(sendID uint64, sendData []byte) error {
	// 首先判断连接是否已经断开 已经断开的连接不发送数据
	if c.Isclosed == true {
		return nil
	}
	// 实例化一个pack对象
	p := NewPack()
	temp := []byte{}
	msg := NewMessage(sendID, sendData)
	temp1, e := p.Pack(msg)
	if e != nil {
		return e
	}
	temp = append(temp, temp1...)

	// 将打包好的数据使用管道发送给写协程
	c.CtransData <- temp
	return nil
}

// 写协程
func (c *Connect) StartWriter() {
	for {
		select {
		case <-c.CheakExitC:
			close(c.CtransData)
			return
		case data := <-c.CtransData:
			if _, e := c.Conn.Write(data); e != nil {
				fmt.Println("[systemLog]:写内容物错误")
				break
			}
		}
	}
}

// 启动 让连接开始工作
func (c *Connect) Start() {
	fmt.Printf("[systemLog]:启动ID:%d连接...\n", c.ConnID)
	// 启动读协程
	go c.StartReader()
	// 启动读协程
	go c.StartWriter()
}

// 停止 结束当前连接的工作
func (c *Connect) Stop() {
	fmt.Printf("[systemLog]:关闭ID:%d连接...\n", c.ConnID)
	if c.Isclosed == true {
		return
	}
	c.Isclosed = true
	// 关闭sokect连接 回收资源
	c.Conn.Close()

	// 使用连接控制器来关闭连接
	c.ConnServer.GetServerConnManager().ConnManDel(c)

	close(c.CheakExitC)
}

// 获取绑定的连接接口
func (c *Connect) Getconn() *net.TCPConn {
	return c.Conn
}

// 获取连接的ID
func (c *Connect) GetConnID() int64 {
	return c.ConnID
}

// 获取连接的状态属性 IP 端口等
func (c *Connect) GetconnInfo() net.Addr {
	return c.Conn.RemoteAddr()
}

// 发送数据 使用连接将二进制流数据发送给客户端
func (c *Connect) SendMsg(msg []byte) error {
	m := "[SystemCallBack]:" + string(msg[:len(string(msg))]) + "\n"
	c.CtransData <- []byte(m)
	return nil
}

// 添加属性
func (c *Connect) InfoAdd(infoIn string, a interface{}) {
	// 写锁
	c.cinfolock.Lock()
	c.cinfo[infoIn] = a
	c.cinfolock.Unlock()
}

// 删除属性
func (c *Connect) InfoDel(infoIn string) {
	// 写锁
	c.cinfolock.Lock()
	delete(c.cinfo, infoIn)
	c.cinfolock.Unlock()

}

// 查询属性
func (c *Connect) InfoFind(infoIn string) interface{} {
	// 读锁
	c.cinfolock.RLock()
	defer c.cinfolock.RUnlock()
	return c.cinfo[infoIn]
}
