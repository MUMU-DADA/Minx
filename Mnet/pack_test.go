package Mnet

import (
	"crypto/sha256"
	"fmt"
	"io"
	"math/rand"
	"net"
	"strconv"
	"testing"
	"time"
)

// 用于测试pack模块是否运行正常的函数
func TestPack(t *testing.T) {
	/*
		创建一个服务器
	*/

	go func() {
		li, e := net.Listen("tcp", "127.0.0.1:7070")
		if e != nil {
			panic(e)
		}
		for {
			conn, e1 := li.Accept()
			if e1 != nil {
				fmt.Println("[server]:服务端创建连接错误")
				continue
			}

			// 连接建立成功
			go func(conn net.Conn) {
				// 开始处理客户端请求

				p := NewPack()
				for {
					// 先读头
					// 建立一个长度为自定义头长度的二进制切片
					temp1 := make([]byte, p.GetPackLen())
					// 将连接中的数据 一次性读满到 切片缓存中
					v, e2 := io.ReadFull(conn, temp1)
					if v == 0 {
						fmt.Println("[server]:读取到二进制流尾部 等待下一次发包")
						return
					}
					if e2 != nil {
						fmt.Println("[server]:读取头字节错误")
						return
					}
					// 获取到读满的字节流中的ID和长度组成的message结构体 其中还未填充结构体中的data内容
					msgHead, e3 := p.UnPack(temp1)
					if e3 != nil {
						fmt.Println("[server]:解包错误")
						return
					}
					// 读取message结构体中的内容的长度 创建字节流切片 再次从conn处读取
					datalen := msgHead.GetLen()
					temp2 := make([]byte, datalen)
					_, err := io.ReadFull(conn, temp2)
					if err != nil {
						fmt.Println("[server]:读取内容物错误")
						return
					}
					// msgHead.(*Message).Msg = temp2

					finalmsg := msgHead.(*Message)
					finalmsg.Msg = make([]byte, datalen)
					finalmsg.Msg = temp2

					fmt.Printf("[server]:ID:%d 长度:%d 内容:%s\n", finalmsg.MsgID, finalmsg.MsgLen, finalmsg.Msg)
				}

			}(conn)

		}
	}()

	/*
		创建一个客户端
	*/

	go func() {
		var abc int = 0
		for {
			time.Sleep(time.Second * 1)
			conn, e := net.Dial("tcp", "127.0.0.1:7070")
			if e != nil {
				panic(e)
			}
			// fmt.Println("客户端创建连接成功")
			go func(conn net.Conn) {
				// fmt.Println("开始进行连接")
				temp := []byte{}
				p := NewPack()
				// 封装若干个包
				for i := 0; i < 3; i++ {
					rd := strconv.Itoa(rand.Intn(1000000))
					ra := sha256.Sum256([]byte(rd))
					var datastr string
					datastr = "这是第" + strconv.Itoa(i) + "个包的数据:" + fmt.Sprintf("%X", ra)[:32]
					// fmt.Println(datastr)
					data := []byte(datastr)
					msg := &Message{
						MsgLen: uint64(len(data)),
						MsgID:  uint64(abc),
						Msg:    data,
					}
					temp1, e := p.Pack(msg)
					if e != nil {
						panic(e)
					}
					temp = append(temp, temp1...)

				}
				fmt.Println("[client]:发送打包数据流...")
				conn.Write(temp)
				defer conn.Close()
				abc++

			}(conn)
		}

	}()

	select {}

}
