package Miface

/*
	用于定义控制连接connManager的接口
*/
type IConnManager interface {
	// 增加连接
	ConnManAdd(Iconnect) error
	// 删除连接
	ConnManDel(Iconnect)
	// 中断所有的连接
	ConnManDelAll()
	// 根据连接ID获取连接
	ConnManGetID(int) Iconnect
	// 获取当前总连接数量
	ConnManGetNum() int
}
