package Miface

// 定义request请求接口
type Irequest interface {
	GetConnect() Iconnect
	GetData() []byte
	GetID() uint64
	GetLen() uint64
}
