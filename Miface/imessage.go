package Miface

/*
	用于定义消息的抽象接口层
*/
type Imessage interface {
	// 获取消息的参数方面

	// 获取消息的长度
	GetLen() uint64
	// 获取消息的ID
	GetID() uint64
	// 获取消息的内容
	GetMsg() []byte

	// 设置消息的参数方面

	// 设置消息的ID
	SetMsgID(uint64)
	// 设置消息的内容
	SetMsg([]byte)
}
