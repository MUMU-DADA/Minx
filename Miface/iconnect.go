package Miface

import "net"

// 定义 连接的接口
type Iconnect interface {
	// 启动 让连接开始工作
	Start()
	// 停止 结束当前连接的工作
	Stop()
	// 获取绑定的连接接口
	Getconn() *net.TCPConn
	// 获取连接的ID
	GetConnID() int64
	// 获取连接的状态属性 IP 端口等
	GetconnInfo() net.Addr
	// 发送数据 使用连接将数据发送给客户端
	SendMsg(msg []byte) error
	// 提供一个发送Message的方法
	SendPackMsg(sendID uint64, sendData []byte) error
	// 添加属性
	InfoAdd(string, interface{})
	// 删除属性
	InfoDel(string)
	// 查询属性
	InfoFind(string) interface{}
}

// 定义处理连接业务的方法

type BangdinFunc func(*net.TCPConn, []byte, int) error
