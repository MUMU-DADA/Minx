package Miface

/*
	定义Message的路由管理模块MsgHandle的抽象层接口
*/
type IMsgHandle interface {
	// 调度路由Router方法
	RouterUse(request Irequest) error
	// 添加路由Router方法
	RouterAdd(msgid uint64, router IRouter) error
	// 创建工作池队列的方法
	StartWorkPool()
	// 将收到的request发送给对应的工作池 并做简易负载均衡
	SendRequestToWorkPool(request Irequest)
}
