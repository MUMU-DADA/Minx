package Miface

// 用于定义路由irouter的抽象接口层
type IRouter interface {
	// 处理业务前
	HandleQian(request Irequest)
	// 处理业务
	Handle(request Irequest)
	// 处理业务后
	HandleHou(request Irequest)
}
