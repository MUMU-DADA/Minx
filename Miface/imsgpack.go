package Miface

/*
	用于定义接受数据的打包方法msgpack的抽象层
*/
type IPack interface {
	// 获取包头部的长度
	GetPackLen(Imessage) uint64
	// 打包
	Pack(Imessage) ([]byte, error)
	// 解包
	UnPack([]byte) (Imessage, error)
}
