package Miface

/*
	此处用于定义接口层
*/

type MServer interface {
	// 启动
	Start()
	// 停止
	Stop()
	// 运行
	Run()
	// 添加一个新路由
	AddNewRouter(addid uint64, router IRouter)
	// 返回服务器所拥有的连接池
	GetServerConnManager() IConnManager
}
