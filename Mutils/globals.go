package Mutils

import (
	"encoding/json"
	"gitee.com/MUMU-DADA/Minx/Miface"
	"io/ioutil"
)

const DEBUGSW bool = false
const VERSION string = "1.0"

/*
	保存读取全局配置 给其他模块直接调用
*/
type GlobalObj struct {
	// server相关
	TcpServer         Miface.MServer // `服务器类型`
	ServerName        string         `json:"服务器名称"`
	ServerIPVer       string         `json:"服务器IP版本"`
	ServerIP          string         `json:"服务器ip地址"`
	ServerPort        int            `json:"服务器端口"`
	ServerRWByteLen   uint32         `json:"服务器读写缓存最大值"`
	ServerMaxConn     int            `json:"服务器最大运行连接数"`
	ServerWorkUnitNum int            `json:"服务器工作单元的最大数量"`
	ServerWorkDuilie  int64          `json:"服务器每个工作单元的工作队列的数量"`

	// 总框架 相关
	DebugSw bool   // '框架debug开关 打开则不读取json脚本数据'
	Version string // `json:"框架版本号"`
}

// 实例化一个全局的GlobalObj对象 供给其他模块使用
var GlobalValue *GlobalObj

// 读取json配置文件
func (g *GlobalObj) LoadJson() {
	data, e := ioutil.ReadFile("conf/conf.json")
	// fmt.Printf("%s\n", data)
	if e != nil {
		panic(e)
	}
	e = json.Unmarshal(data, &GlobalValue)
	if e != nil {
		panic(e)
	}

}

func init() {
	// 创建一个
	GlobalValue := &GlobalObj{
		ServerRWByteLen: 4096,
		Version:         VERSION,
		DebugSw:         DEBUGSW,
	}
	if GlobalValue.DebugSw == true {
		GlobalValue.ServerName = "MinxApp"
		GlobalValue.ServerIP = "0.0.0.0"
		GlobalValue.ServerIPVer = "tcp4"
		GlobalValue.ServerPort = 7070
		GlobalValue.ServerRWByteLen = 4096
		GlobalValue.ServerWorkUnitNum = 10
		GlobalValue.ServerWorkDuilie = 100
		GlobalValue.ServerMaxConn = 500

	} else {
		// 开始调用读取Json 填充配置文件
		GlobalValue.LoadJson()
	}

}
